# Indexing engines

## Description
This project contains several packages, written in Rust, designated for creation of indexes for digital resources in plain text or XML format.

The format of the indexes is RDF (Turtle).

### Word level segmentation

* https://docs.rs/unicode-segmentation/latest/unicode_segmentation/

* https://github.com/rust-lang/triagebot/blob/master/parser/src/token.rs

* https://docs.rs/tokenate/latest/tokenate/

* https://petermalmgren.com/token-scanning-with-rust/

### Sentence level segmentation

* https://lib.rs/crates/unic-segment

* https://docs.rs/unic-segment/latest/unic_segment/

* https://github.com/bminixhofer/srx

* https://stackoverflow.com/questions/57029974/how-to-split-string-into-chunks-in-rust-to-insert-spaces

* https://lib.rs/crates/charabia

* https://lib.rs/crates/logos-cli

* https://lib.rs/crates/pragmatic-segmenter

* https://lib.rs/crates/cutters

### Morpho-syntactic annotation

* https://github.com/UniversalDependencies/docs/issues/344

* https://hindawi.com/journals/sp/2017/7831897/

* https://github.com/justinwilaby/sax-wasm

* https://lib.rs/crates/santiago
