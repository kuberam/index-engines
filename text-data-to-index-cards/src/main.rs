use rio_api::formatter::TriplesFormatter;
use rio_api::model::{Literal, NamedNode, Triple};
use rio_turtle::TurtleFormatter;
use std::collections::{HashMap, HashSet};
use std::io::{BufRead, BufReader};
use std::path::{Path, PathBuf};
use std::{env, fs};

use crate::tokenizer::{Token, Tokenizer, WhitespaceTokenizer};

mod tokenizer;

static INDEX_CARDS_FILE_EXTENSION: &str = "idxc";

fn main() {
    // get the base dir path
    let args: Vec<String> = env::args().collect();
    let text_files_base_path = Path::new(&args[1]);
    let text_files_glob =
        glob::Pattern::new(&args[2]).expect("wrong glob pattern");
    let indexes_dir = Path::new(&args[3]);

    generate_index_cards_for_files(
        text_files_base_path,
        text_files_glob,
        indexes_dir,
    );
}

fn generate_index_cards_for_files(
    text_files_base_path: &Path,
    text_files_glob: glob::Pattern,
    indexes_dir: &Path,
) {
    dbg!(text_files_base_path);
    let text_files_path = text_files_base_path.join(text_files_glob.as_str());
    let mut global_cross_references: Vec<String> = Vec::new();

    for (text_file_ordinal_number, text_file_path) in
        glob::glob(text_files_path.to_str().unwrap())
            .expect("Failed to read glob pattern")
            .filter_map(Result::ok)
            .enumerate()
    {
        // create dirs
        let index_cards_dir = indexes_dir.join("index-cards");
        fs::create_dir_all(&index_cards_dir).expect("cannot create dir");

        // generate the relative file path
        let text_file_relative_path =
            get_relative_path(text_files_base_path, &text_file_path);

        let _file_index_cards = generate_index_cards_for_file(
            text_file_path,
            &text_file_relative_path,
            index_cards_dir,
            text_file_ordinal_number,
        );

        global_cross_references.push(text_file_relative_path);
    }

    fs::write(
        indexes_dir.join("relative-paths-to-documents.json"),
        serde_json::to_string(&global_cross_references)
            .expect("Serialize file."),
    )
    .expect("Write file.");
}

fn generate_index_cards_for_file(
    text_file_path: PathBuf,
    relative_text_url: &str,
    index_cards_dir: PathBuf,
    _file_ordinal_number: usize,
) -> Vec<String> {
    // get the file contents
    let file_contents: String = fs::read_to_string(text_file_path)
        .expect("cannot read file")
        .parse()
        .expect("cannot parse file");

    // get the contents' tokens
    let file_tokens =
        WhitespaceTokenizer.tokenize(&file_contents).collect::<Vec<Token>>();

    // generate the index cards
    let mut token_data: HashMap<&str, String> = HashMap::new();
    let mut index_cards: Vec<String> = Vec::new();
    file_tokens.iter().for_each(|file_token| {
        let token = file_token.term();
        let start_offset: String = file_token.start_offset();
        let position: String = file_token.position();

        token_data
            .entry(token)
            .and_modify(|entry| {
                *entry += format!(",{}:{}", start_offset, position).as_str()
            })
            .or_insert(format!("{}:{}", start_offset, position));
    });

    token_data.iter().for_each(|token_datum| {
        index_cards.push(format!("\t{}\t{}", token_datum.1, token_datum.0));
    });

    // write the index cards file
    let mut index_cards_file_path = index_cards_dir.join(relative_text_url);
    let index_cards_dir_path = index_cards_file_path.parent().unwrap();

    fs::create_dir_all(&index_cards_dir_path).expect("cannot create dir");

    index_cards_file_path.set_extension(INDEX_CARDS_FILE_EXTENSION);

    let index_cards_file_contents =
        format!("#{}\n{}", relative_text_url, &index_cards.join("\n"));

    fs::write(&index_cards_file_path, &index_cards_file_contents)
        .expect("Write file.");

    index_cards
}

fn get_relative_path(
    text_files_base_path: &Path,
    text_file_path: &PathBuf,
) -> String {
    let mut text_file_relative_path_buffer = PathBuf::new();
    text_file_path
        .components()
        .skip(text_files_base_path.components().collect::<Vec<_>>().len())
        .for_each(|component| text_file_relative_path_buffer.push(component));
    let text_file_relative_path =
        text_file_relative_path_buffer.to_str().unwrap();

    text_file_relative_path.to_owned()
}

#[test]
fn test_generate_index_cards_for_file() {
    let text_files_base_path = &Path::new(
        "/home/claudius/workspace/repositories/git/github.com/wujastyk/INDOLOGY-forum-data/data/",
    );
    let text_files_glob =
        glob::Pattern::new("_test/new-test/01-00003.txt").unwrap();
    let text_file_path = text_files_base_path.join(text_files_glob.as_str());

    let text_file_relative_path =
        get_relative_path(text_files_base_path, &text_file_path);

    let indexes_dir = Path::new("/home/claudius/workspace/repositories/git/github.com/wujastyk/INDOLOGY-forum-data/public/");
    let index_cards_dir = indexes_dir.join("index-cards");

    generate_index_cards_for_file(
        text_file_path,
        &text_file_relative_path,
        index_cards_dir,
        1,
    );
}

#[test]
fn test_generate_index_cards_for_files() {
    let text_files_base_path = &Path::new(
        "/home/claudius/workspace/repositories/git/github.com/wujastyk/INDOLOGY-forum-data/data/",
    );
    let text_files_glob = glob::Pattern::new("**/*.txt").unwrap();
    let indexes_dir = Path::new("/home/claudius/workspace/repositories/git/github.com/wujastyk/INDOLOGY-forum-data/public/");

    generate_index_cards_for_files(
        text_files_base_path,
        text_files_glob,
        indexes_dir,
    );
}

#[test]
fn generate_triple_files() {
    let text_files_base_path = Path::new("/home/claudius/workspace/repositories/git/github.com/wujastyk/INDOLOGY-forum-data/data/");
    let text_files_glob =
        glob::Pattern::new("**/*.txt").expect("wrong glob pattern");
    let text_files_path = text_files_base_path.join(text_files_glob.as_str());

    let indexes_dir = Path::new("/home/claudius/workspace/repositories/git/github.com/wujastyk/INDOLOGY-forum-data/public/datasets/");

    let mut global_terms: HashMap<String, String> = HashMap::new();
    let mut text_relative_iri_formatter = TurtleFormatter::new(Vec::default());
    let mut terms_formatter = TurtleFormatter::new(Vec::default());

    for (file_ordinal_number, text_file_path) in
        glob::glob(text_files_path.to_str().unwrap())
            .expect("Failed to read glob pattern")
            .filter_map(Result::ok)
            .enumerate()
    {
        dbg!(&text_file_path);

        // initialize the structure for terms with aggregated positions
        let mut aggregated_file_terms: HashMap<String, String> =
            HashMap::new();

        // generate the relative file path and add it to the global struct
        let text_file_relative_path =
            get_relative_path(text_files_base_path, &text_file_path);
        text_relative_iri_formatter
            .format(&Triple {
                subject: NamedNode { iri: text_file_relative_path.as_str() }
                    .into(),
                predicate: NamedNode { iri: "dc:identifier" }.into(),
                object: Literal::Simple {
                    value: file_ordinal_number.to_string().as_str(),
                }
                .into(),
            })
            .expect("error formatting text relative IRIs triple");

        // get the file contents
        let file_contents: String = fs::read_to_string(&text_file_path)
            .expect("cannot read file")
            .parse()
            .expect("cannot parse file");

        // get the contents' tokens
        let file_tokens = WhitespaceTokenizer
            .tokenize(&file_contents)
            .collect::<Vec<Token>>();

        // aggregate the tokens
        file_tokens.iter().for_each(|file_token| {
            let term = file_token.term();
            let position = file_token.position();
            aggregated_file_terms
                .entry(term.to_owned())
                .and_modify(|entry| {
                    *entry = format!("{},{}", *entry, position);
                })
                .or_insert(format!("{}:[{}", file_ordinal_number, position));
        });

        // add the file terms to global terms' struct
        aggregated_file_terms.iter().for_each(|file_term| {
            let term = file_term.0;
            let mut positions = file_term.1.to_string();

            if !positions.ends_with("]") {
                positions = format!("{}]", positions);
            }

            global_terms
                .entry(term.to_owned())
                .and_modify(|entry| {
                    *entry = format!("{},{}", *entry, positions);
                })
                .or_insert(format!("{}", positions));
        })
    }

    // generate the triples file for text relative IRIs
    let mut text_relative_iri_prolog = format!("{} .\n{} .\n", "@base <https://wujastyk.github.io/INDOLOGY-forum-data/data/>", "@prefix dc: <http://purl.org/dc/terms/>").as_bytes().to_vec();
    let mut text_relative_iri_document = text_relative_iri_formatter
        .finish()
        .expect("cannot finish text relative IRIs generation");
    text_relative_iri_prolog.append(&mut text_relative_iri_document);
    fs::write(
        indexes_dir.join("text-relative-iris.ttl"),
        &text_relative_iri_prolog,
    )
    .expect("Write file.");

    // generate the triples file for terms
    let mut terms_prolog = format!("{} .\n{} .\n", "@base <https://wujastyk.github.io/INDOLOGY-forum-data/indexes/words-fulltext/>", "@prefix bibo: <http://purl.org/ontology/bibo/>").as_bytes().to_vec();
    global_terms.iter().for_each(|global_term| {
        terms_formatter
        .format(&Triple {
            subject: NamedNode { iri: global_term.0.as_str() }
                .into(),
            predicate: NamedNode { iri: "bibo:locator" }.into(),
            object: Literal::Simple {
                value: format!("{{{}}}", global_term.1).as_str(),
            }
            .into(),
        })
        .expect("error formatting text relative IRIs triple");        
    });
    let mut terms_document = terms_formatter
        .finish()
        .expect("cannot finish terms generation");
    terms_prolog.append(&mut terms_document);
    fs::write(
        indexes_dir.join("terms.ttl"),
        &terms_prolog,
    )
    .expect("Write file.");    
}

// time cargo build --bin text-data-to-index-cards --target-dir ./target --release --target x86_64-unknown-linux-musl
// time cargo run --bin text-data-to-index-cards --target-dir ./target -- "/home/claudius/workspace/repositories/git/github.com/wujastyk/INDOLOGY-forum-data/data/**/*.txt"
// time ./text-data-to-index-cards "/home/claudius/workspace/repositories/git/github.com/wujastyk/INDOLOGY-forum-data/data/**/*.txt" "/home/claudius/workspace/repositories/git/github.com/wujastyk/INDOLOGY-forum-data/public/indexes/text/"
// https://nitschinger.at/Text-Analysis-in-Rust-Tokenization/

// initiate the lemmatizer
/*let lemmatizer: Lemmatizer = Lemmatizer::new(
    "../generate-en-language-tools/flexionary-forms-and-lemmas/flexionary-forms.fst",
    "../generate-en-language-tools/flexionary-forms-and-lemmas/lemmas.txt",
);*/
// self.lemmatizer.get_key(lowercased_slice.as_str()).unwrap_or(&lowercased_slice),
