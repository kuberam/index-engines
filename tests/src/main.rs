use ngram_search::Ngrams;
use serde_json::Value;
use std::fs;
use std::io::BufWriter;
use std::path;
use ngrammatic;

fn main() {}

#[test]
fn ngram_string() {
    let ngrams = ngrammatic::NgramBuilder::new("sanskits")
        .arity(2)
        .pad_full(ngrammatic::Pad::Pad("_".to_string()))
        .finish();    

    println!("{:?}", ngrams);
}

#[test]
fn ngram_corpus() {
    let mut corpus = ngrammatic::CorpusBuilder::new()
    .arity(2)
    .pad_full(ngrammatic::Pad::Auto)
    .finish();

    // Build up the list of known words
    corpus.add_text("sanskrit");
    corpus.add_text("yesterday");

    // Now we can try an unknown/misspelled word, and find a similar match
    // in the corpus
    let word = String::from("sanskits");
    if let Some(top_result) = corpus.search(&word, 0.1).first() {
        println!("{} (did you mean {}? [{:.0}% match])",
        word,
        top_result.text,
        top_result.similarity * 100.0);
    } else {
        println!("🗙 {}", word);
    }    
}

#[test]
fn ngram_search_build_index() {
    // read the words
    let words_file = path::Path::new("/home/claudius/workspace/repositories/git/github.com/wujastyk/INDOLOGY-forum-data/public/indexes/word-ngrams/words.json");
    let words_string =
        fs::read_to_string(words_file).expect("Unable to read file.");
    let words_value: Value = serde_json::from_str(&words_string)
        .expect("Unable to convert to array.");
    let words = words_value.as_array().unwrap();

    // path to index file
    let index_path = path::Path::new("index.ngram");

    // Build index
    let mut builder = Ngrams::builder();
    for (i, word) in words.iter().enumerate() {
        let word_as_str = word.as_str().unwrap();
        builder.add(word_as_str, i.try_into().unwrap());
    }

    // Write it to a file
    let mut file = BufWriter::new(fs::File::create(&index_path).unwrap());
    builder.write(&mut file).unwrap();
}
#[test]
fn ngram_search_search_index() {
    // read the words
    let words_file = path::Path::new("/home/claudius/workspace/repositories/git/github.com/wujastyk/INDOLOGY-forum-data/public/indexes/word-ngrams/words.json");
    let words_string =
        fs::read_to_string(words_file).expect("Unable to read file.");
    let words_value: Value = serde_json::from_str(&words_string)
        .expect("Unable to convert to array.");
    let words = words_value.as_array().unwrap();

    // path to index file
    let index_path = path::Path::new("index.ngram");

    // Search our index
    let mut data = Ngrams::open(&index_path).unwrap();
    if let Ok(result_1) = data.search("sanskits", 0.3) {
        for (i, item) in result_1.iter().enumerate() {
            println!(
                "{}. word: {}, similarity: {}",
                i,
                words[usize::try_from(item.0).unwrap()],
                item.1
            );
        }
    }
}

// time cargo run --bin tests --target-dir ./target -- "./test-data/indexes/text/"
