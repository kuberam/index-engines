use qwt::QWT256;
use std::fs;
use std::path::Path;
use qwt::AccessUnsigned;

pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
#[test]
fn qwt_1() {
    let data = vec![1u8, 0, 1, 0, 2, 4, 5, 3];

    let qwt = QWT256::from(data);
    
    assert_eq!(qwt.len(), 8);
}

#[test]
fn qwt_2() {
    let data = vec![1u8, 0, 1, 0, 2, 4, 5, 3];
    let qwt = QWT256::from(data);
    
    assert_eq!(qwt.get(2), Some(1));
    
    // serialize
    let serialized = bincode::serialize(&qwt).unwrap();
    
    // write on file, if needed
    let output_filename = "example.qwt256".to_string();
    fs::write(Path::new(&output_filename), serialized).unwrap();
    
    // read from file 
    let serialized = fs::read(Path::new(&output_filename)).unwrap();
    
    // deserialize
    let qwt = bincode::deserialize::<QWT256<u8>>(&serialized).unwrap();
    
    assert_eq!(qwt.get(2), Some(1));    
}
