use fst::MapBuilder;
use lexical_sort::{natural_lexical_cmp, StringSort};
use regex_cache::LazyRegex;
use serde::{Deserialize, Serialize};
use std::collections::{BTreeMap, HashMap};
use std::{io, path};
use aho_corasick::AhoCorasick;

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub enum IndexType {
    Fst,
    Json,
    Fulltext,
    Ngram,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct FstIndexDefinition {
    pub index_type: IndexType,
    pub allowed_letters: String,
    pub letter_replacements: Vec<Vec<String>>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct JsonIndexDefinition {
    pub index_type: IndexType,
    pub entry_names: Vec<String>,
    pub index_name: String,
    pub index_regex: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct FullTextIndexDefinition {
    pub index_type: IndexType,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct NgramIndexDefinition {
    pub index_type: IndexType,
    pub q: usize,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(untagged)]
pub enum GeneralIndexDefinition {
    FstIndexDefinition(FstIndexDefinition),
    JsonIndexDefinition(JsonIndexDefinition),
    FullTextIndexDefinition(FullTextIndexDefinition),
    NgramIndexDefinition(NgramIndexDefinition),
}

#[derive(Serialize, Deserialize, Debug)]
pub struct FstInvertedIndex {
    pub entries: Vec<String>,
    pub allowed_letters: String,
    pub letter_replacements: Vec<Vec<String>>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct JsonInvertedIndex {
    pub facets: Vec<String>,
    pub labels: String,
    pub regex: String,
    pub indexes: HashMap<String, FacetInvertedIndex>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct FullTextInvertedIndex {
    pub indexes: HashMap<String, Vec<usize>>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct NgramInvertedIndex {
    pub indexes: HashMap<String, Vec<usize>>,
    pub q: usize,
    pub tokens: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum GeneralInvertedIndex {
    FstInvertedIndex(FstInvertedIndex),
    JsonInvertedIndex(JsonInvertedIndex),
    FullTextInvertedIndex(FullTextInvertedIndex),
    NgramInvertedIndex(NgramInvertedIndex),
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct FacetInvertedIndex {
    pub inverted_index: Vec<u32>,
    values: HashMap<String, Vec<u32>>,
}

impl JsonInvertedIndex {
    pub fn add(&mut self, facet_values: Vec<String>, record_index: u32) {
        for facet_value in facet_values.into_iter() {
            let mut indexed: bool = false;
            let regex = &self.regex;

            for facet in &self.facets {
                let facet_record = self.indexes.get_mut(facet).expect("Get facet record.");

                if regex == "starts_with" && facet_value.starts_with(facet) {
                    let facet_inverted_index = &mut facet_record.inverted_index;
                    facet_inverted_index.push(record_index);

                    let facet_value_index: u32 = (facet_inverted_index.len() - 1) as u32;

                    facet_record
                        .values
                        .entry(facet_value.to_owned())
                        .and_modify(|entry| {
                            entry.push(facet_value_index);
                        })
                        .or_insert(vec![facet_value_index]);

                    indexed = true;
                }

                if regex == "contains" && facet_value.contains(facet) {
                    let facet_inverted_index = &mut facet_record.inverted_index;
                    facet_inverted_index.push(record_index);

                    let facet_value_index: u32 = (facet_inverted_index.len() - 1) as u32;

                    facet_record
                        .values
                        .entry(facet_value.to_owned())
                        .and_modify(|entry| {
                            entry.push(facet_value_index);
                        })
                        .or_insert(vec![facet_value_index]);

                    indexed = true;
                }
            }

            if !indexed {
                // add inverted index for the new facet
                let mut new_inverted_index = FacetInvertedIndex::new();
                new_inverted_index.add(facet_value.to_string(), record_index);

                self.indexes
                    .insert(facet_value.to_string(), new_inverted_index);

                // add facet_value as new facet
                self.facets.push(facet_value);
            }
        }
    }
}

impl FacetInvertedIndex {
    pub fn new() -> Self {
        let inverted_index = Vec::new();
        let values: HashMap<String, Vec<u32>> = HashMap::new();

        Self {
            inverted_index,
            values,
        }
    }

    fn add(&mut self, value: String, record_index: u32) {
        self.inverted_index.push(record_index);
        self.values.insert(value, vec![0]);
    }
}

pub fn generate_fst_index(
    entries: Vec<String>,
    index_dir_path: path::PathBuf,
    allowed_letters: String,
    letter_replacements: Vec<Vec<String>>,
) {
    let mut combined_entries: BTreeMap<String, Vec<usize>> = BTreeMap::new();

    let mut first_letters: Vec<String> = Vec::<String>::new();

    let fst_file_handle =
        std::fs::File::create(&index_dir_path.join("index.fst")).expect("Create the FST file.");
    let fst_buffered_writer = io::BufWriter::new(fst_file_handle);

    let mut map_builder = MapBuilder::new(fst_buffered_writer).expect("Generate map builder.");

    // generate the index for words
    for (i, entry) in entries.iter().enumerate() {
        let mut entry = entry.to_owned().to_lowercase();

        // check for allowed characters
        let allowed_letters_regex: LazyRegex = LazyRegex::new(&allowed_letters).unwrap();

        for letter in allowed_letters_regex.captures_iter(&entry) {
            panic!("Not allowed letter {:?}", &letter[0]);
        }

        // replace some characters or not
        let ac = AhoCorasick::new(&letter_replacements[0]);
        entry = ac.replace_all(&entry, &letter_replacements[1]);

        // get the first letter
        let first_letter = entry.chars().nth(0).unwrap().to_string();
        first_letters.push(first_letter);

        // add the entry to the index
        combined_entries
            .entry(entry.clone())
            .and_modify(|value| {
                if let None = value.get(i) {
                    value.push(i);
                }
            })
            .or_insert(vec![i]);
    }

    // generate the FST for words
    for (i, entry) in combined_entries.iter().enumerate() {
        map_builder.insert(&entry.0, i as u64).unwrap();
    }
    map_builder
        .finish()
        .expect("Finish generation of the FST map.");

    // generate the file with words
    std::fs::write(
        index_dir_path.join("words.json"),
        format!("{:?}", combined_entries.clone().into_keys()),
    )
    .expect("Write file.");

    // generate the cross reference pointers for words
    let cross_reference_pointers: Vec<_> = combined_entries.into_values().collect::<_>();
    std::fs::write(
        index_dir_path.join("index.json"),
        format!("{:?}", cross_reference_pointers),
    )
    .expect("Write file.");

    // generate the file with first letters
    first_letters.string_sort_unstable(natural_lexical_cmp);
    first_letters.dedup();
    let sorted_first_letters: Vec<String> = first_letters
        .iter()
        .map(|item| item.to_uppercase())
        .collect();
    std::fs::write(
        index_dir_path.join("first-letters.json"),
        serde_json::to_string(&sorted_first_letters).expect("Serialize file."),
    )
    .expect("Write file.");
}

pub fn serialize_index_to_file(index_dir_path: path::PathBuf, inverted_index: JsonInvertedIndex) {
    let mut facet_values: Vec<String> = Vec::new();

    for (entry_value, entry_inverted_index) in inverted_index.indexes.iter() {
        facet_values.push(entry_value.to_string());

        let processed_entry_value = encode_string(entry_value.to_string());

        std::fs::write(
            index_dir_path.join(processed_entry_value.to_owned() + ".json"),
            serde_json::to_string(&entry_inverted_index).expect("Serialize file."),
        )
        .expect("Write file.");
    }
}

// this will have to be URL encode
pub fn encode_string(string_to_encode: String) -> String {
    string_to_encode.replace(".", "").replace(" ", "")
}

/*
impl InvertedIndex {
    pub fn new() -> InvertedIndex {
        let facets = HashMap::from([
            (
                "adj.".to_string(), HashMap::from([
                    ("adj.".to_string(), Vec::<u32>::new()),
                    ("adj. dem.".to_string(), Vec::<u32>::new()),
                    ("adj. inter.-rel.".to_string(), Vec::<u32>::new()),
                    ("adj. invar.".to_string(), Vec::<u32>::new()),
                    ("adj. nehot.".to_string(), Vec::<u32>::new()),
                    ("adj. pos.".to_string(), Vec::<u32>::new()),
                ])
            ),
            (
                "s.".to_string(), HashMap::from([
                    ("s.".to_string(), Vec::<u32>::new()),
                    ("s. f.".to_string(), Vec::<u32>::new()),
                    ("s. m.".to_string(), Vec::<u32>::new()),
                    ("s. n.".to_string(), Vec::<u32>::new()),
                    ("s. pr.".to_string(), Vec::<u32>::new()),
                    ("s. f. pl.".to_string(), Vec::<u32>::new()),
                    ("s. m. pl.".to_string(), Vec::<u32>::new()),
                    ("s. f. art.".to_string(), Vec::<u32>::new()),
                ])
            ),
            (
                "vb.".to_string(), HashMap::from([
                    ("vb.".to_string(), Vec::<u32>::new()),
                    ("vb. I".to_string(), Vec::<u32>::new()),
                    ("vb. II".to_string(), Vec::<u32>::new()),
                    ("vb. III".to_string(), Vec::<u32>::new()),
                    ("vb. IV".to_string(), Vec::<u32>::new()),
                ])
            )
        ]);
    }

    pub fn add(&mut self, facet_values: Vec<String>, record_index: u32) {
        for facet_value in facet_values.into_iter() {
            match facet_value.as_ref() {
                "adj." => self.facets.get_mut("adj.").expect("Get facet.").get_mut("adj.").expect("Get facet value.").push(record_index),
                "adj. invar." => {
                    self.facets.get_mut("adj.").expect("Get facet.").get_mut("adj.").expect("Get facet value.").push(record_index);
                    let facet_value_index: u32 = (self.facets.get_mut("adj.").expect("Get facet.").get_mut("adj.").expect("Get facet value.").len() - 1).try_into().expect("facet value index {}");
                    self.facets.get_mut("adj.").expect("Get facet.").get_mut("adj. invar.").expect("Get facet value.").push(facet_value_index);
                },
                "adv." => self.facets.get_mut("adv.").expect("Get facet.").get_mut("adv.").expect("Get facet value.").push(record_index),
                //"art." => self.art.push(record_index),
                //"conj." => self.conj.push(record_index),
                "interj." => self.facets.get_mut("interj.").expect("Get facet.").get_mut("interj.").expect("Get facet value.").push(record_index),
                "loc. adj." => self.facets.get_mut("loc. adj.").expect("Get facet.").get_mut("loc. adj.").expect("Get facet value.").push(record_index),
                "loc. adv." => self.facets.get_mut("loc. adv.").expect("Get facet .").get_mut("loc. adv.").expect("Get facet value.").push(record_index),
                //"num." => self.num.push(record_index),
                "prep." => self.facets.get_mut("prep.").expect("Get facet.").get_mut("prep.").expect("Get facet value.").push(record_index),
                //"pron." => self.pron.push(record_index),
                "s." => self.facets.get_mut("s.").expect("Get facet.").get_mut("s.").expect("Get facet value.").push(record_index),
                "s. f." | "f." => {
                    self.facets.get_mut("s.").expect("Get facet.").get_mut("s.").expect("Get facet value.").push(record_index);
                    self.facets.get_mut("s.").expect("Get facet.").get_mut("s. f.").expect("Get facet value.").push(record_index);
                },
                "s. m." => {
                    self.facets.get_mut("s.").expect("Get facet.").get_mut("s.").expect("Get facet value.").push(record_index);
                    self.facets.get_mut("s.").expect("Get facet.").get_mut("s. m.").expect("Get facet value.").push(record_index);
                },
                "s. n." => {
                    self.facets.get_mut("s.").expect("Get facet.").get_mut("s.").expect("Get facet value.").push(record_index);
                    self.facets.get_mut("s.").expect("Get facet.").get_mut("s. n.").expect("Get facet value.").push(record_index);
                },
                "s. f. pl." => {
                    self.facets.get_mut("s.").expect("Get facet.").get_mut("s.").expect("Get facet value.").push(record_index);
                    self.facets.get_mut("s.").expect("Get facet.").get_mut("s. f. pl.").expect("Get facet value.").push(record_index);
                },
                "s. m. pl." => {
                    self.facets.get_mut("s.").expect("Get facet.").get_mut("s.").expect("Get facet value.").push(record_index);
                    self.facets.get_mut("s.").expect("Get facet.").get_mut("s. m. pl.").expect("Get facet value.").push(record_index);
                },
                "s. f. art." => {
                    self.facets.get_mut("s.").expect("Get facet.").get_mut("s.").expect("Get facet value.").push(record_index);
                    self.facets.get_mut("s.").expect("Get facet.").get_mut("s. f. art.").expect("Get facet value.").push(record_index);
                },
                "vb." => self.facets.get_mut("vb.").expect("Get facet.").get_mut("vb.").expect("Get facet value.").push(record_index),
                "vb. I" => {
                    self.facets.get_mut("vb.").expect("Get facet.").get_mut("vb.").expect("Get facet value.").push(record_index);
                    self.facets.get_mut("vb.").expect("Get facet.").get_mut("vb. I").expect("Get facet value.").push(record_index);
                },
                "vb. II" => {
                    self.facets.get_mut("vb.").expect("Get facet.").get_mut("vb.").expect("Get facet value.").push(record_index);
                    self.facets.get_mut("vb.").expect("Get facet.").get_mut("vb. II").expect("Get facet value.").push(record_index);
                },
                "vb. III" => {
                    self.facets.get_mut("vb.").expect("Get facet.").get_mut("vb.").expect("Get facet value.").push(record_index);
                    self.facets.get_mut("vb.").expect("Get facet.").get_mut("vb. III").expect("Get facet value.").push(record_index);
                },
                "vb. IV" => {
                    self.facets.get_mut("vb.").expect("Get facet.").get_mut("vb.").expect("Get facet value.").push(record_index);
                    self.facets.get_mut("vb.").expect("Get facet.").get_mut("vb. IV").expect("Get facet value.").push(record_index);
                },
                _ => panic!("unknown facet value <<{}>>", facet_value),
            }
        }
    }
}
 */

//  {
//     "adj": {
//       "locations": {},
//       "regexp": /^adj\.$/
//     },
//     "adv": {
//       "locations": {},
//       "regexp": /^adv\.$/
//     },
//     "art": {
//       "locations": {},
//       "regexp": /^art\./
//     },
//     "conj": {
//       "locations": {},
//       "regexp": /.*conj.*/
//     },
//     "interj": {
//       "locations": {},
//       "regexp": /.*interj.*/
//     },
//     "locadj": {
//       "locations": {},
//       "regexp": /^loc\. adj\.$/
//     },
//     "locadv": {
//       "locations": {},
//       "regexp": /^loc\. adv\.$/
//     },
//     "num": {
//       "locations": {},
//       "regexp": /.*num.*/
//     },
//     "prep": {
//       "locations": {},
//       "regexp": /.*prep.*/
//     },
//     "pron": {
//       "locations": {},
//       "regexp": /.*pron.*/
//     },
//     "s": {
//       "locations": {},
//       "regexp": /.*[s\.|subst\.].*/
//     },
//     "vb": {
//       "locations": {},
//       "regexp": /.*vb.*/
//     },
//   };
