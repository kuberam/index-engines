use lexical_sort::{natural_lexical_cmp, StringSort};
use ngrammatic;
use serde::Serialize;
use std::collections::HashMap;
use std::fmt::Write as _;
use std::io::{self, BufRead};
use std::{env, fs, path};

use crate::structs::indexes;

mod structs;

// the minimal number of columns in index cards' file
static MINIMAL_NUMBER_OF_INDEX_CARDS_COLUMNS: usize = 2;

fn main() {
    // get the base dir path
    let args: Vec<String> = env::args().collect();

    let index_cards_files_path = &args[1];
    let indexes_dir_path = path::Path::new(&args[2]);

    dbg!(&args);

    process_index_cards_files(index_cards_files_path, indexes_dir_path);
}

fn process_index_cards_files(
    index_cards_files_path: &String,
    indexes_dir_path: &path::Path,
) {
    let mut number_of_entries: u32 = 0;
    let mut relative_paths_to_documents: Vec<String> = Vec::new();

    // get and validate the index definitions
    let indexes_definitions_data =
        fs::read_to_string(indexes_dir_path.join("indexes-config.json"))
            .expect("Unable to read file");
    let indexes_definitions: indexmap::IndexMap<
        String,
        indexes::GeneralIndexDefinition,
    > = serde_json::from_str(&indexes_definitions_data)
        .expect("err: unable to parse json");

    // create the structs for all the inverted indexes
    let mut inverted_indexes: indexmap::IndexMap<
        String,
        indexes::GeneralInvertedIndex,
    > = indexmap::IndexMap::new();
    indexes_definitions
        .iter()
        .for_each(|(index_name, index_definition)| match index_definition {
            indexes::GeneralIndexDefinition::FstIndexDefinition(current_index_definition) => {
                let allowed_letters_formatted = format!(
                    r"{}",
                    regex::escape(&current_index_definition.allowed_letters.clone())
                );
                let fst_inverted_index: indexes::FstInvertedIndex = indexes::FstInvertedIndex {
                    entries: Vec::<String>::new(),
                    allowed_letters: allowed_letters_formatted,
                    letter_replacements: current_index_definition.letter_replacements.clone(),
                };
                inverted_indexes.insert(
                    index_name.to_string(),
                    indexes::GeneralInvertedIndex::FstInvertedIndex(fst_inverted_index),
                );
            }
            indexes::GeneralIndexDefinition::JsonIndexDefinition(current_index_definition) => {
                let indexes = HashMap::from(
                    current_index_definition
                        .entry_names
                        .iter()
                        .map(|facet| (facet.to_string(), indexes::FacetInvertedIndex::new()))
                        .collect::<HashMap<String, indexes::FacetInvertedIndex>>(),
                );
                let json_inverted_index: indexes::JsonInvertedIndex = indexes::JsonInvertedIndex {
                    facets: current_index_definition.entry_names.clone(),
                    labels: current_index_definition.index_name.clone(),
                    regex: current_index_definition.index_regex.clone(),
                    indexes,
                };
                inverted_indexes.insert(
                    index_name.to_string(),
                    indexes::GeneralInvertedIndex::JsonInvertedIndex(json_inverted_index),
                );
            }
            indexes::GeneralIndexDefinition::FullTextIndexDefinition(_current_index_definition) => {
                let indexes: HashMap<String, Vec<usize>> = HashMap::new();
                let full_text_inverted_index: indexes::FullTextInvertedIndex =
                    indexes::FullTextInvertedIndex { indexes };
                inverted_indexes.insert(
                    index_name.to_string(),
                    indexes::GeneralInvertedIndex::FullTextInvertedIndex(full_text_inverted_index),
                );
            }
            indexes::GeneralIndexDefinition::NgramIndexDefinition(current_index_definition) => {
                let indexes: HashMap<String, Vec<usize>> = HashMap::new();
                let tokens: Vec<String> = Vec::new();
                let ngram_inverted_index: indexes::NgramInvertedIndex =
                    indexes::NgramInvertedIndex {
                        indexes,
                        q: current_index_definition.q,
                        tokens,
                    };
                inverted_indexes.insert(
                    index_name.to_string(),
                    indexes::GeneralInvertedIndex::NgramInvertedIndex(ngram_inverted_index),
                );
            }

        });

    // create the structs for the cross references
    let mut cross_references: Vec<Vec<String>> = Vec::new();

    // create the variables and structs needed for processing the index cards
    let indexes_number =
        inverted_indexes.len() + MINIMAL_NUMBER_OF_INDEX_CARDS_COLUMNS;
    let index_abbreviated_names: Vec<String> =
        inverted_indexes.keys().map(|item| item.to_string()).collect();
    let mut _index_cards_files_number: usize = 0;

    for (index_cards_file_ordinal_number, index_cards_file_path) in
        glob::glob(index_cards_files_path)
            .expect("Failed to read glob pattern")
            .filter_map(Result::ok)
            .enumerate()
    {
        // increment counter
        //index_cards_files_number += 1;

        //dbg!(&index_cards_file_path);
        // check is path is URL
        // https://crates.io/crates/is-url

        // process the index cards
        if let Ok(lines) = read_index_records(index_cards_file_path) {
            for (i, line) in lines.iter().enumerate() {
                if line.starts_with("#") {
                    relative_paths_to_documents.push(line.replace("#", ""));

                    continue;
                }
                let tokens: Vec<String> =
                    line.split("\t").map(|s| s.to_string()).collect();
                let tokens_number = tokens.len();

                if tokens_number != indexes_number {
                    panic!("Index record `{line}` should have {indexes_number} fields, got {tokens_number}.");
                }

                let record_index: _ = i as u32;

                // extract the metadata for the cross reference
                cross_references
                    .push(vec![tokens[0].to_string(), tokens[1].to_string()]);

                if tokens[2].len() == 0 {
                    panic!("Index record `{line}` has an empty field.");
                }

                // for each line, loop over the indexes, so that each one will collect its token
                // (based upon the order in the index configuration file)
                for (j, index_abbreviated_name) in
                    index_abbreviated_names.iter().enumerate()
                {
                    let current_inverted_index = inverted_indexes
                        .get_mut(index_abbreviated_name)
                        .expect("Get index.");
                    let current_index_ordinal =
                        j + MINIMAL_NUMBER_OF_INDEX_CARDS_COLUMNS;

                    match current_inverted_index {
                        indexes::GeneralInvertedIndex::FstInvertedIndex(
                            current_index_definition,
                        ) => {
                            current_index_definition
                                .entries
                                .push(tokens[current_index_ordinal].to_string());
                        }
                        indexes::GeneralInvertedIndex::JsonInvertedIndex(
                            current_index_definition,
                        ) => {
                            current_index_definition.add(
                                CategoryVector::new(
                                    &line,
                                    index_abbreviated_name.to_string(),
                                    tokens[current_index_ordinal].to_string(),
                                ),
                                record_index,
                            );
                        }
                        indexes::GeneralInvertedIndex::FullTextInvertedIndex(
                            current_index_definition,
                        ) => {
                            let current_token = tokens[current_index_ordinal].to_string();
                            current_index_definition
                                .indexes
                                .entry(current_token.to_owned())
                                .and_modify(|entry| {
                                    entry.push(index_cards_file_ordinal_number);
                                })
                                .or_insert(vec![index_cards_file_ordinal_number]);
                        }
                        indexes::GeneralInvertedIndex::NgramInvertedIndex(
                            current_index_definition,
                        ) => {
                            let current_token = tokens[current_index_ordinal].to_string();
                            let index_tokens: &mut Vec<String> = &mut  current_index_definition.tokens;

                            // check if current token is already processed
                            if !index_tokens.contains(&current_token) {
                                let current_token_ngrams = ngrammatic::NgramBuilder::new(current_token.as_str())
                                .arity(current_index_definition.q)
                                .pad_full(ngrammatic::Pad::Pad("_".to_string()))
                                .finish();
                                let current_index = index_tokens.len();
                                index_tokens.push(current_token);

                                for current_token_ngram in current_token_ngrams.grams.keys() {
                                    current_index_definition
                                    .indexes
                                    .entry(current_token_ngram.to_owned())
                                    .and_modify(|entry| {
                                        entry.push(current_index);
                                    })
                                    .or_insert(vec![current_index]);
                                }
                            }
                        }
                    }
                }

                number_of_entries += 1;
            }
        }
    }

    // serialize in JSON format the cross references for indexes
    // headword_records.sort_by(|a, b| a.partial_cmp(b).unwrap());
    std::fs::write(
        indexes_dir_path.join("cross-references.json"),
        serde_json::to_string(&cross_references).expect("Serialize file."),
    )
    .expect("Write file.");

    // serialize in JSON format the indexes' metadata and the inverted indexes, respectively
    let mut categories_to_serialize = CategoriesDefinition {
        definitions: indexmap::IndexMap::new(),
        number_of_entries: number_of_entries,
    };
    for (index_abbreviated_name, current_inverted_index) in
        inverted_indexes.iter_mut()
    {
        let index_dir_path = indexes_dir_path.join(index_abbreviated_name);
        std::fs::create_dir_all(&index_dir_path).expect("Create dir.");

        match current_inverted_index {
            indexes::GeneralInvertedIndex::FstInvertedIndex(
                current_inverted_index,
            ) => {
                // generate the FST index
                indexes::generate_fst_index(
                    current_inverted_index.entries.clone(),
                    index_dir_path,
                    current_inverted_index.allowed_letters.clone(),
                    current_inverted_index.letter_replacements.clone(),
                );
            }
            indexes::GeneralInvertedIndex::JsonInvertedIndex(
                current_inverted_index,
            ) => {
                let mut facets: Vec<FacetValueDefinition> = Vec::new();
                let indexes = &current_inverted_index.indexes;

                current_inverted_index
                    .facets
                    .string_sort_unstable(natural_lexical_cmp);
                for facet_value in
                    current_inverted_index.facets.clone().into_iter()
                {
                    let occurences: u32 = indexes
                        .get(&facet_value)
                        .expect("Get facet record.")
                        .inverted_index
                        .len()
                        as u32;

                    let facet_definition = FacetValueDefinition {
                        value: facet_value.clone(),
                        path: format!(
                            "{}/{}",
                            index_abbreviated_name,
                            indexes::encode_string(facet_value)
                        ),
                        occurences: occurences,
                    };

                    facets.push(facet_definition);
                }

                let category_metadata = CategoryDefinition {
                    labels: current_inverted_index.labels.to_owned(),
                    facets: facets,
                };

                categories_to_serialize.definitions.insert(
                    index_abbreviated_name.to_string(),
                    category_metadata,
                );

                indexes::serialize_index_to_file(
                    index_dir_path,
                    current_inverted_index.clone(),
                );
            }
            indexes::GeneralInvertedIndex::FullTextInvertedIndex(
                current_inverted_index,
            ) => {
                for (entry_value, entry_inverted_index) in
                    current_inverted_index.indexes.iter()
                {
                    // generate the array with document id-s
                    /*let mut index_cards_files_bitarray: Vec<usize> = vec![0; index_cards_files_number];
                    for entry in entry_inverted_index {
                        index_cards_files_bitarray[*entry] = 1;
                    }*/

                    // generate the path to index file
                    let current_dir_path = &index_dir_path;
                    let first_letter =
                        entry_value.chars().nth(0).unwrap().to_string();
                    let mut current_dir_path =
                        current_dir_path.join(first_letter);
                    let second_letter = entry_value.chars().nth(1);
                    match second_letter {
                        Some(letter) => {
                            current_dir_path =
                                current_dir_path.join(letter.to_string());
                        }
                        None => (),
                    }
                    fs::create_dir_all(&current_dir_path).unwrap();

                    std::fs::write(
                        current_dir_path
                            .join(entry_value.to_owned() + ".json"),
                        serde_json::to_string(&entry_inverted_index)
                            .expect("Serialize file."),
                    )
                    .expect("Write file.");
                }
            }
            indexes::GeneralInvertedIndex::NgramInvertedIndex(
                current_inverted_index,
            ) => {
                for (ngram, word_indexes) in
                    current_inverted_index.indexes.iter()
                {
                    let mut current_ngram_indexes = word_indexes.to_owned();
                    current_ngram_indexes.sort_unstable();
                    current_ngram_indexes.dedup();

                    // serialize the word ngrams
                    std::fs::write(
                        index_dir_path.join(ngram.to_owned() + ".json"),
                        serde_json::to_string(&current_ngram_indexes)
                            .expect("Serialize file."),
                    )
                    .expect("Write file.");

                    // serialize the words
                    std::fs::write(
                        index_dir_path.join("words.json"),
                        serde_json::to_string(&current_inverted_index.tokens)
                            .expect("Serialize file."),
                    )
                    .expect("Write file.");
                }
            }
        }
    }
    std::fs::write(
        indexes_dir_path.join("indexes-metadata.json"),
        serde_json::to_string(&categories_to_serialize)
            .expect("Serialize file."),
    )
    .expect("Write file.");

    // serialize the relative paths to documents
    fs::write(
        indexes_dir_path.join("relative-paths-to-documents.json"),
        serde_json::to_string(&relative_paths_to_documents)
            .expect("Serialize file."),
    )
    .expect("Write file.");

    // serialize in HTML format the categories metadata and the inverted indexes, respectively
    let mut categories_to_html = String::new();
    write!(
        &mut categories_to_html,
        r#"
            <div class="ac">
                <h2 class="ac-header">
                    <button class="ac-trigger">{}</button>
                </h2>
                <div class="ac-panel"></div>
            </div>
        "#,
        "abc"
    )
    .expect("Serialize categories.");
}

#[derive(Debug, Serialize)]
struct CategoriesDefinition {
    definitions: indexmap::IndexMap<String, CategoryDefinition>,
    number_of_entries: u32,
}

#[derive(Debug, Serialize)]
struct CategoryDefinition {
    labels: String,
    facets: Vec<FacetValueDefinition>,
}

#[derive(Debug, Serialize)]
struct FacetValueDefinition {
    value: String,
    path: String,
    occurences: u32,
}

struct CategoryVector;

impl CategoryVector {
    pub fn new(
        index_record: &String,
        index_category_name: String,
        index_category_value: String,
    ) -> Vec<String> {
        let input_vector: Vec<String> = index_category_value
            .split("|")
            .map(|item| item.to_string())
            .collect();
        let initial_length = input_vector.len();
        let mut output_vector: Vec<String> = input_vector
            .into_iter()
            .filter(|element| !element.is_empty())
            .collect();
        if &output_vector.len() < &(initial_length - 1) {
            panic!(
                "The field <<{}>> of the index card\n{}\ngot missing value(s).",
                index_category_name, index_record
            );
        }
        output_vector.sort_unstable();
        output_vector.dedup();

        output_vector
    }
}

fn read_index_records<P>(filename: P) -> io::Result<Vec<String>>
where
    P: AsRef<path::Path>,
{
    let file = fs::File::open(filename)?;
    let mut lines: Vec<String> =
        io::BufReader::new(file).lines().map(|line| line.unwrap()).collect();
    lines.sort();

    Ok(lines)
}

pub fn remove_test_files(test_dir_path: &path::PathBuf) {
    let indexes_dir_path = test_dir_path.join("indexes");

    // remove indexes dir
    match fs::remove_dir_all(&indexes_dir_path) {
        _ => (),
    }

    // create indexes dir
    fs::create_dir_all(&indexes_dir_path).unwrap();

    // copy the indexes configuration file
    fs::copy(
        test_dir_path.join("indexes-config.json"),
        indexes_dir_path.join("indexes-config.json"),
    )
    .unwrap();
}

#[test]
fn test_1() {
    let test_dir_path = env::current_dir()
        .unwrap()
        .as_path()
        .join("test-data/set-1")
        .canonicalize()
        .unwrap();

    remove_test_files(&test_dir_path);

    let index_cards_file_path =
        test_dir_path.join("index-cards/index-cards.idxc");

    // process the index cards file
    process_index_cards_files(
        &index_cards_file_path.as_path().to_str().unwrap().to_string(),
        test_dir_path.join("indexes").as_path(),
    );
}

#[test]
fn ngram_index_few_files() {
    let test_dir_path = env::current_dir()
        .unwrap()
        .as_path()
        .join("test-data/set-2")
        .canonicalize()
        .unwrap();

    remove_test_files(&test_dir_path);

    let index_cards_files_path = path::PathBuf::from("/home/claudius/workspace/repositories/git/github.com/wujastyk/INDOLOGY-forum-data/public/index-cards/1990/").join("**/*.idxc");

    // process the index cards file
    process_index_cards_files(
        &index_cards_files_path.as_path().to_str().unwrap().to_string(),
        test_dir_path.join("indexes").as_path(),
    );
}

#[test]
fn ngram_index_many_files() {
    let test_dir_path = env::current_dir()
        .unwrap()
        .as_path()
        .join("test-data/set-2")
        .canonicalize()
        .unwrap();

    remove_test_files(&test_dir_path);

    let index_cards_files_path = path::PathBuf::from("/home/claudius/workspace/repositories/git/github.com/wujastyk/INDOLOGY-forum-data/public/index-cards/").join("**/*.idxc");

    // process the index cards file
    process_index_cards_files(
        &index_cards_files_path.as_path().to_str().unwrap().to_string(),
        test_dir_path.join("indexes").as_path(),
    );
}

#[test]
fn fst_index_few_files() {
    let test_dir_path = env::current_dir()
        .unwrap()
        .as_path()
        .join("test-data/set-3")
        .canonicalize()
        .unwrap();

    remove_test_files(&test_dir_path);

    let index_cards_files_path = path::PathBuf::from("/home/claudius/workspace/repositories/git/github.com/wujastyk/INDOLOGY-forum-data/public/index-cards/new-test/*.idxc");

    // process the index cards file
    process_index_cards_files(
        &index_cards_files_path.as_path().to_str().unwrap().to_string(),
        test_dir_path.join("indexes").as_path(),
    );
}

#[test]
fn test_fulltext_index_2() {
    let test_dir_path = env::current_dir()
        .unwrap()
        .as_path()
        .join("test-data/set-3")
        .canonicalize()
        .unwrap();

    remove_test_files(&test_dir_path);

    let index_cards_files_path = path::PathBuf::from("/home/claudius/workspace/repositories/git/gitlab.com/solirom-cflr/A100058/public/index-cards/").join("**/*.idxc");

    // process the index cards file
    process_index_cards_files(
        &index_cards_files_path.as_path().to_str().unwrap().to_string(),
        test_dir_path.join("indexes").as_path(),
    );
}

// generate the index for each facet value in HTML, based upon a template that can be replaced at client side

// check uniqueness of entries, based upon: lexical category | headword | homonym number | gramatical category

// improve the terminology: index-cards (heading / subheading, locators); index

// CategoryVector has to disappear when xml-data-to-index-cards is done

// dedicated docker container for the above index engines (data-to-indexes),
// multiple functions for indexing: xml-data-to-index-cards, text-data-to-index-cards, index-cards-to-indexes
// (https://levelup.gitconnected.com/create-an-optimized-rust-alpine-docker-image-1940db638a6c)

// RESOURCES
// https://usethe.computer/posts/14-xmhell.html
// validate structs, https://github.com/Keats/validator, https://docs.rs/valid/latest/valid/
// generate structs from config file, https://docs.rs/config_struct/latest/config_struct/
// XML parser, https://github.com/RazrFalcon/roxmltree

// serialization to XML
// https://github.com/BurntSushi/rust-csv/blob/master/src/serializer.rs
// https://docs.rs/yaserde/0.8.0/yaserde/

// command line
// https://github.com/BurntSushi/fst/blob/master/fst-bin/src/cmd/set.rs

// hash functions
// https://nnethercote.github.io/2021/12/08/a-brutally-effective-hash-function-in-rust.html

// time cargo build --bin index-cards-to-indexes --target-dir ./target --release --target x86_64-unknown-linux-musl
// time cargo run --bin index-cards-to-indexes --target-dir ./target -- "./test-data/indexes/text/"
// cargo build --path .
// time index-cards-to-indexes "/home/claudius/workspace/repositories/git/github.com/wujastyk/INDOLOGY-forum-data/public/index-cards/**/*.idxc" "/home/claudius/workspace/repositories/git/gitlab.com/solirom/index-engines/index-cards-to-indexes/test-data/set-2"
// index-cards-to-indexes "/home/claudius/workspace/repositories/git/github.com/wujastyk/INDOLOGY-forum-data/public/indexes/text/index-cards/**/*.idxc" "/home/claudius/workspace/repositories/git/gitlab.com/solirom/index-engines/index-cards-to-indexes/test-data/set-2/indexes"

// sudo gitlab-ci-local && sudo chown -R claudius:claudius .
