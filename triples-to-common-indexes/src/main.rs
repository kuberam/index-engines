use csv;
use fst::MapBuilder;
use lexical_sort::{natural_lexical_cmp, StringSort};
use reqwest::blocking::Client;
use std::{
    collections::BTreeMap,
    env, fs, io,
    path::{self, Path},
};

fn main() {
    // get the args
    let args: Vec<String> = env::args().collect();
    let datasets_description_file_path: &path::Path =
        path::Path::new(&args[1]);
    let indexes_dir_path: &path::Path = path::Path::new(&args[2]);

    process_datasets(datasets_description_file_path, indexes_dir_path);
}

fn process_datasets(
    datasets_description_file_path: &path::Path,
    indexes_dir_path: &path::Path,
) {
    dbg!(datasets_description_file_path);
    dbg!(indexes_dir_path);

    // create the indexes dir, if it does not exist
    if !indexes_dir_path.is_dir() {
        let _ = fs::create_dir_all(indexes_dir_path);
    }

    // reads the dataset descriptions
    let mut datasets_description_reader = csv::ReaderBuilder::new()
        .has_headers(false)
        .comment(Some(b'#'))
        .from_path(datasets_description_file_path)
        .expect("cannot read file");

    // initializations
    let mut index: BTreeMap<String, Vec<Vec<String>>> = BTreeMap::new();
    let http_client = Client::new();

    // process each dataset description
    for result in datasets_description_reader.records() {
        let record = result.unwrap();

        // get the metadata about datasets
        let dataset_url = record[0].to_string();
        let text_siglum = &record[1];

        // determinate the delimiters for the dataset contents
        let mut record_delimiter = b'\t';
        if dataset_url.ends_with("positional-index.txt") {
            record_delimiter = b'|';
        }

        // get the dataset
        let dataset_contents = http_client
            .get(dataset_url)
            .send()
            .expect("cannot execute the request")
            .text()
            .expect("cannot get contents");

        process_dataset(
            dataset_contents,
            record_delimiter,
            &mut index,
            text_siglum,
        );
    }

    // initialize the s-fst index
    let s_fst_dir_path = &indexes_dir_path.join("s-fst");
    fs::create_dir_all(&s_fst_dir_path).unwrap();
    let s_fst_file_handle =
        std::fs::File::create(&s_fst_dir_path.join("index.fst"))
            .expect("Create the FST file.");
    let s_fst_buffered_writer = io::BufWriter::new(s_fst_file_handle);
    let mut s_fst_map_builder =
        MapBuilder::new(s_fst_buffered_writer).expect("Generate map builder.");
    let mut first_letters: Vec<String> = Vec::<String>::new();

    for (ordinal, (subject, inverted_index)) in index.iter().enumerate() {
        // generate the so-json-remote-fs index
        let ordinal_string = ordinal.to_string();
        let current_dir_path = &indexes_dir_path.join("so-json-remote-fs");
        let absolute_dir_path =
            calculate_relative_dir_path(current_dir_path, &ordinal_string);
        fs::create_dir_all(&absolute_dir_path).unwrap();
        std::fs::write(
            absolute_dir_path.join(ordinal_string + ".json"),
            serde_json::to_string(&inverted_index).expect("Serialize file."),
        )
        .expect("Write file.");

        // generate the s-fst index
        s_fst_map_builder.insert(subject, ordinal as u64).unwrap();
        let first_letter = subject.chars().nth(0).unwrap().to_string();
        first_letters.push(first_letter);
    }

    // finish the s-fst index
    s_fst_map_builder.finish().expect("Finish generation of the FST map.");
    first_letters.string_sort_unstable(natural_lexical_cmp);
    first_letters.dedup();
    let sorted_first_letters: Vec<String> =
        first_letters.iter().map(|item| item.to_uppercase()).collect();
    std::fs::write(
        s_fst_dir_path.join("first-letters.json"),
        serde_json::to_string(&sorted_first_letters).expect("Serialize file."),
    )
    .expect("Write file.");

    // generate the s-json-remote-fs index
    let s_json_remote_fs = &indexes_dir_path.join("s-json-remote-fs");
    fs::create_dir_all(&s_json_remote_fs).unwrap();
    std::fs::write(
        s_json_remote_fs.join("index.json"),
        format!("{:?}", &index.keys()),
    )
    .expect("Write file.");
}

fn calculate_relative_dir_path<'a>(
    current_dir_path: &'a path::PathBuf,
    ordinal_string: &'a String,
) -> path::PathBuf {
    let first_letter = ordinal_string.chars().nth(0).unwrap().to_string();
    let mut current_dir_path = current_dir_path.join(first_letter);
    let second_letter = ordinal_string.chars().nth(1);
    match second_letter {
        Some(letter) => {
            current_dir_path = current_dir_path.join(letter.to_string());
        }
        None => (),
    }
    let third_letter = ordinal_string.chars().nth(2);
    match third_letter {
        Some(letter) => {
            current_dir_path = current_dir_path.join(letter.to_string());
        }
        None => (),
    }

    current_dir_path
}

fn process_dataset(
    dataset_contents: String,
    record_delimiter: u8,
    index: &mut BTreeMap<String, Vec<Vec<String>>>,
    text_siglum: &str,
) {
    let mut dataset_contents_reader = csv::ReaderBuilder::new()
        .has_headers(false)
        .delimiter(record_delimiter)
        .from_reader(dataset_contents.as_bytes());

    for result in dataset_contents_reader.records() {
        // get the record
        let record = result.unwrap();

        // get the needed fields (headword+homonym number+part of speech and relative IRI of text)
        let mut entry_title = record[0].to_owned();
        let mut entry_headword = process_headword(&record[2]);
        let mut entry_full_iri = &record[1];
        let mut homonym_number = "".to_string();

        if record_delimiter == b'|' {
            if let Some(i) =
                entry_title.chars().filter(|x| x.is_ascii_digit()).next()
            {
                homonym_number = i.to_string();
                entry_title = entry_title.replace(homonym_number.as_str(), "");
            }

            let splitted_entry_title =
                entry_title.split("\\").collect::<Vec<&str>>();
            let mut raw_entry_headword = splitted_entry_title[0];
            if splitted_entry_title[1].len() > 0 {
                raw_entry_headword = splitted_entry_title[1];
            }

            let processed_entry_title_1 = format!(
                "{}|{}|{}",
                raw_entry_headword, homonym_number, &record[1]
            );

            entry_headword = process_headword(raw_entry_headword);
            entry_title = processed_entry_title_1;
            entry_full_iri = &record[2];
        }

        // process the fields
        entry_headword = entry_headword.replace("\u{a0}", " ");
        entry_title = entry_title.replace("\u{a0}", " ");
        let entry_reference = vec![
            text_siglum.to_string(),
            entry_title,
            entry_full_iri.to_string(),
        ];

        index
            .entry(entry_headword.to_string())
            .and_modify(|entry| {
                entry.push(entry_reference.clone());
            })
            .or_insert(vec![entry_reference]);
    }
}

fn process_headword(headword: &str) -> String {
    let mut result = headword
        .to_lowercase()
        .replace(" ", " ")
        .trim()
        .replace("á", "a")
        .replace("à", "a")
        .replace("ä", "a")
        .replace("é", "e")
        .replace("è", "e")
        .replace("í", "i")
        .replace("ì", "i")
        .replace("ǐ", "i")
        .replace("ó", "o")
        .replace("ò", "o")
        .replace("ú", "u")
        .replace("ù", "u")
        .replace("ü", "u")
        .replace("ắ", "ă")
        .replace("ấ", "â")
        .replace("î́", "î")
        .replace("î\u{341}", "î")
        .replace("ç", "c")
        .replace("ñ", "n")
        .replace("ô", "o")
        .replace("ý", "y")
        .replace(", -ă", "")
        .replace(", -ească", "")
        .replace(", -oare", "")
        .replace(", -oasă", "")
        .replace(", -ee", "")
        .replace(", -ie", "")
        .replace(", -oagă", "")
        .replace(", -ea", "")
        .replace(", -ică", "")
        .replace(", -oală", "")
        .replace(", -urie", "")
        .replace(", -oară", "")
        .replace(", -oaltă", "")
        .replace(", -șartă", "")
        .replace(", -oaie", "")
        .replace(", -ce", "")
        .replace(", -uă", "")
        .replace(", -iască", "")
        .replace(", -oacă", "")
        .replace(", -zecea", "")
        .replace(", -oață", "")
        .replace(", -oașă", "")
        .replace(", -cie", "")
        .replace(", -oamă", "")
        .replace(", -iască", "")
        .replace(", -oană", "")
        .replace(", -âie", "")
        .replace(", -oace", "")
        .replace(", -oază", "")
        .replace(", -ască", "")
        .replace(", -oabă", "")
        .replace(", -toare", "")
        .replace(", -să", "")
        .replace(", -iață", "")
        .replace(", -ore", "")
        .replace(", -arie", "")
        .replace(", -îie", "")
        .replace(", -orie", "")
        .replace(", -ghe", "")
        .replace(", -aie", "")
        .replace(", -uie", "")
        .replace(", -oapă", "")
        .replace(", -opă", "")
        .replace(", -oadă", "")
        .replace(", -iastră", "")
        .replace(", -che", "")
        .replace(", -oana", "")
        .replace(", -a", "")
        .replace(", -e", "")
        .replace(", -oată", "")
        .replace(", -oarsă", "")
        .replace(", -oaptă", "")
        .replace(", -pea", "")
        .replace(", -rea", "")
        .replace(", -uoasă", "")
        .replace(", -iță", "")
        .replace(", -ta", "")
        .replace(", -oaică", "")
        .replace(", -zeacea", "")
        .replace(", -oanță", "")
        .replace(", -udză", "")
        .replace(", -direaptă", "")
        .replace(", -dreaptă", "")
        .replace(", -ogă", "")
        .replace(", -veche", "")
        .replace(", -oane", "")
        .replace(", -oavă", "")
        .replace(", -toarie", "")
        .replace(", -oambă", "")
        .replace(", -neagră", "")
        .replace(", -grea", "")
        .replace(", -oastă", "")
        .replace(", -ită", "")
        .replace(", -oafă", "")
        .replace(", -oancă", "")
        .replace(", -oache", "")
        .replace(", -roiasă", "")
        .replace(",", "");

    if result.ends_with(",") {
        result.pop();
    }

    result
}

pub fn remove_index_files(indexes_dir_path: &path::PathBuf) {
    // remove indexes dir
    match fs::remove_dir_all(&indexes_dir_path) {
        _ => (),
    }

    // create indexes dir
    fs::create_dir_all(&indexes_dir_path).unwrap();
}

#[test]
fn process_datasets_test() {
    let datasets_description_file_path = env::current_dir()
        .unwrap()
        .as_path()
        .join("test-data/datasets-descriptions.csv")
        .canonicalize()
        .unwrap();
    let indexes_dir_path = env::current_dir()
        .unwrap()
        .as_path()
        .join("test-data/indexes")
        .canonicalize()
        .unwrap();
    remove_index_files(&indexes_dir_path);

    process_datasets(
        datasets_description_file_path.as_path(),
        indexes_dir_path.as_path(),
    );
}

#[test]
fn process_external_dataset() {
    let datasets_description_file_path = Path::new("/home/claudius/workspace/repositories/git/gitlab.com/solirom-clre/texts/dl/backend/public/datasets-descriptions.csv");
    let indexes_dir_path = Path::new("/home/claudius/workspace/repositories/git/gitlab.com/solirom-clre/texts/dl/backend/public/indexes/");
    remove_index_files(&indexes_dir_path.to_path_buf());

    process_datasets(datasets_description_file_path, indexes_dir_path);
}
